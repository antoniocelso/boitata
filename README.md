# Boitatá - Digestor Anaeróbico de resto alimentares - Open Hardware
Projeto baseado em Ciência Cidadã, para mais detalhes acesse: boitata.eco.br

## Crotalus - Boitatá experimental

- [x] Medidor volumétrico - Totalizador tipo 0.6l de GN, conectado via switch relay;
- [x] Temperatura - dois DS18B20 (interno e externo)  

- [ ] Analisador espectral de chama - em construção;
- [ ] Temperatura nas diversas areas do biodigestor - pesquisa;
- [ ] Medição pH fluxo contínuo - pesquisa;
- [ ] Medição Ácidos Graxos e Carbonos inorgânicos (A/CIT) - pesquisa;

### Processamento
Usando bibliotecas arduino, código simples (arduino IDE);
Testado em ESP32

### Comunicação 
Usando FTP, em transição para CoAP, problema com conexão intermintente;

### Medidor Volume - 

#### Totalizador
Usando o totalizador de GN.

#### Contador de bolhas - Não funcional
Usando air lock, arduino Uno, eletrodos de grafite, conectado ao arduino (Porta 3/INPUT_PULLUP), veja no site para mais detalhes.
Pela simplicidade e baixo custo opta-se pelo arduino Uno, no entanto o código é muito simples e pode ser compilado para outras placas.

Opções via serial:

l- retorna o acumulado bolhas (tipo long - cerca de 2 bilhões de bolhas), desde a última inicialização;

z- inicializa a variável.


Lista a fazer:
* Otimizar funções;
* Verificar tempo de gravação compatível com sensor;

