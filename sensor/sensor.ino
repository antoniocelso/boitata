/* Firmware medidor de volume biodigestor Boitata
* EEPROM - jmanatee@ardunino.cc
* atractorz@gmail.com
*/

#include <EEPROM.h>


#define DEBUG 1

int sensor =3;
int led_teste = 5;
int contagem = 0;
int ant_contagem =0;
int ch_sensor = HIGH;
int st_sensor = HIGH;
char input_key;

int address = 0;
long acumulado;

void setup() {
  
	pinMode(sensor,INPUT_PULLUP);
	pinMode(led_teste, OUTPUT);
	Serial.begin(9600);
	Serial.println("digite l - retorna o acumulado");
	Serial.println("digite z - inicializa acumulado");



}

void loop() {
  	
	check_sensor();
	check_key();
	check_contagem();
	
} //fim do loop

/*Verifica e altera contagem, escreve na EEPROM

*/
void check_contagem(){
	if(ant_contagem < contagem){
		eeprom_increment();
	} 
	
	contagem = 0;
	ant_contagem =0;
}//fim check_contagem

/*Checa se alguma tecla foi apertada e toma providencias
*/
void check_key(){
	if(Serial.available()>0){
		input_key=Serial.read();		
	}
	else input_key=NULL;

	switch (input_key){
		case 'l': // leia a contagem
			Serial.print("Contagem ");
			Serial.println(EEPROMReadlong(address));		
		break;

		case 'z':
			EEPROMZero(address);
		break;		
		
	}

}//fim check_key


/*Verifica o status do sensor e altera e toma providencia se necessario
*/
void check_sensor(){
	st_sensor = digitalRead(sensor); // guarda status do sensor
	

	if(st_sensor == LOW){
		digitalWrite(led_teste, HIGH); 	
	}
	
	if(ch_sensor < st_sensor){ // vai contabilizar as mudança de estado, quando regrassar pro estado natural
	contagem ++;
   
  	}
  	else{
    
		digitalWrite(led_teste, LOW);
  	}
  
	
	ch_sensor = st_sensor; 

}//fim do check_sensor
