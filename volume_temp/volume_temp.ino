/*
 *Sistema de controle do Boitatá 
 *Suporte á switch relay - Medidor de volume - 0.6L
 *Dois DS18b20 (ambiente e centro boitatá
 *Port para esp32
 */

#include <WiFi.h>
#include "FS.h"
#include "SPIFFS.h"
#include <SimpleFTPServer.h>
#include "time.h"
#include <OneWire.h>


const char* ssid = "ssid";
const char* password = "password";

IPAddress local_IP(192, 168, 0, 7); // setando ip estatico
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 0, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional


const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = -10800; // fuso -3*60*60
const int   daylightOffset_sec = 0; // horario de verao


char str_data_hora[64];
struct tm timeinfo; //bibliteca time.h nativa do esp

const char* arquivo = "/log.txt"; // armazenamento dos dados em sequência cronológica.


FtpServer ftpSrv;   //set #define FTP_DEBUG in ESP8266FtpServer.h to see ftp verbose on serial

OneWire  ds(17);

double long intervalo_term = 120000;  // 1 leitura cada 2 min - 120000
double long tempo_term = 0;

const int medidas_temp = 20; // anota a cada 10 contagem (10 temp_ext+ 10 temp_int)
int cont_medidas = 0;
float total_int=0;
float total_ext=0;


static uint32_t d_timer = 0;
const uint32_t debounce = 1000;

struct Sensor {
  const uint8_t PIN;
  uint32_t numberkeypresses;
  bool pressed;
};

Sensor sensor1 = {18,0,false};

/*
 * isr: função que lida com a interrupção do sensor de volume IRAM_ATTR indica para ficar na RAM
 * modifica  sensor1.pressed e nunberkeypresses
 */
void IRAM_ATTR isr() { //seta para memória RAM

  if(millis()- d_timer > debounce){
       sensor1.numberkeypresses += 1;
       sensor1.pressed = true;
       d_timer = millis();
  }
  
}//fim ISR



/*
 *Funcao: atuakstrdatahora - Atualiza a cadeia str_data_hora[64]
 *Modifica: a string
 *ToDo: melhorar caso não seja possível atualizar a hora
 */
int atualstrdatahora(){
   
  if(!getLocalTime(&timeinfo)){
    Serial.println("data/Hora desatualizada");
  }
   strftime(str_data_hora, 64 ,"%d/%m/%Y %H:%M", &timeinfo);
   Serial.println(str_data_hora);
   return 1;
}// fim atualstrdatahora

/*
 * funcao appendFile: Se tiver um arquivo ele adciona no final a mensagem 
 */
void appendFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
        Serial.println("Message appended");
    } else {
        Serial.println("Append failed");
    }
    file.close();
} //fim appendFile


/*
 * le_temperatura: acessa os dois sensores e anota o valor.
 */

void le_temperatura(void){

  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  
  byte interno = 222;
  byte externo = 102;

  char* termometro; 
  
  
  float celsius, fahrenheit;
  
  while(ds.search(addr)){
    //if(addr[1] == interno) termometro = "temp_int";
    //if(addr[1] == externo) termometro = "temp_ext";
    
    ds.reset();
    ds.select(addr);
    ds.write(0x44, 1);        // start conversion, with parasite power on at the end
    delay(1000); // maybe 750ms is enough, maybe not we might do a ds.depower() here, but the reset will take care of it.
    present = ds.reset();
    ds.select(addr);    
    ds.write(0xBE);         // Read Scratchpad

    for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    //Serial.print(data[i], HEX);
    //Serial.print(" ");
  }
 
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
      raw = raw << 3; // 9 bit resolution default
      if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
      byte cfg = (data[4] & 0x60);
      // at lower res, the low bits are undefined, so let's zero them
     if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
      else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
      else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
      //// default is 12 bit resolution, 750 ms conversion time
  }
    celsius = (float)raw / 16.0;
    //fahrenheit = celsius * 1.8 + 32.0;
    cont_medidas ++;

    if(addr[2] == interno){ 
      termometro = "temp_int";
      total_int = total_int + celsius;
    }
    if(addr[2] == externo){
      termometro = "temp_ext";
      total_ext = total_ext + celsius;
      
    }


    
   if(atualstrdatahora()&& cont_medidas >= medidas_temp){
      char strappend[100];
      
      float temp_int = total_int/((float)medidas_temp/2.0); // código espaguetoni! precisa deixar mais bonito isso! cada 2 vezes que ele entra 1 le o int ou ext
      float temp_ext = total_ext/((float)medidas_temp/2.0);
      
      Serial.print("Temp_int ");
      Serial.println(temp_int);
      Serial.print("Temp_ext ");
      Serial.println(temp_ext);
      
      sprintf(strappend,"%s|Temp_int|%f\n%s|Temp_ext|%f\n", str_data_hora,temp_int,str_data_hora,temp_ext); 
     
      appendFile(SPIFFS,arquivo,strappend);
      total_int = 0;
      total_ext = 0;
      cont_medidas = 0;
  }
   
   
   
   /* if(atualstrdatahora()){
    char strappend[100];
    sprintf(strappend,"%s %s %s\n", str_data_hora, termometro,celsius); 
    appendFile(SPIFFS,arquivo,strappend);
  }
  */
    Serial.print(termometro);
    Serial.print(" temperatura = ");
    Serial.print(celsius);
    Serial.println(" Celsius ");   
  } // fim while
  
 
 ds.reset_search();
 delay(250);
 return;
  
}//fim le_temperatura


void sensor_acionado(){
   
   if(atualstrdatahora()){
    char strappend[100];
    sprintf(strappend,"%s|volume|1|total|%d \n", str_data_hora,sensor1.numberkeypresses); 
    appendFile(SPIFFS,arquivo,strappend);
    Serial.println("Sensor acionado");
  }
}

void setup(void){
  Serial.begin(115200);
  
  pinMode(sensor1.PIN, INPUT_PULLUP);
  attachInterrupt(sensor1.PIN, isr, FALLING);

  
  WiFi.begin(ssid, password);
  Serial.println("");
 
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) { 
    Serial.println("STA falhou em estabelecer o ipfixo");
  }
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());



  if (SPIFFS.begin(true)) {
    Serial.println("SPIFFS opened!");
    ftpSrv.begin("esp8266","esp8266");    //username, password for ftp.  set ports in ESP8266FtpServer.h  (default 21, 50009 for PASV)
  }  

    //init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
 
  if(atualstrdatahora()){
    char strappend[100];
    sprintf(strappend,"%s reiniciou \n", str_data_hora); 
    appendFile(SPIFFS,arquivo,strappend);
  }
   
} //fim void setup


void loop(void){
  ftpSrv.handleFTP();        //make sure in loop you call handleFTP()!!  
  
  if(sensor1.pressed){
  sensor_acionado();
  sensor1.pressed = false;
 }
 
 
 if(millis()-tempo_term > intervalo_term){
  le_temperatura();
  tempo_term = millis();
 }
 
} // fim loop
